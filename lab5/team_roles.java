//Jacob Shick and Meaghan Wilby
//Lab 5 Team Roles
//
//
//
// Developing the main idea and grid was done together in class where we developed the grid on a small scale including the random number generator, usage of the ascii tables and the way the message was scanned. 

// Meaghan took these plans and expanded it out to the 400 variables we needed to construct the grid and did a great job doing so in a very organized way. She also constructed the grid and formatted it to look right in the output window.

//Jacob delt with the string manipulation, making sure the message was capitalized and padded so the user could input any length message so long as it contains no numbers. He then cut the message up into its individual charicaters and stratigically placed them within the grid.

// We were both responsible for bug and error fixes.
